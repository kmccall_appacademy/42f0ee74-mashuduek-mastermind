PEGS = {
  "R" => 'red',
  "G" => 'green',
  "B" => 'blue',
  "O" => 'orange',
  "Y" => 'yellow',
  "P" => 'purple'
}
require 'byebug'
class Code
  attr_reader :pegs

  def self.parse(input)
    pegs = []
    input.split(//).each do |key|
      pegs << PEGS[key.upcase]
      raise "THATS NOT A COLOR DUMMIE" if !PEGS.include?(key.upcase)
    end
    self.new(pegs)
  end

  def self.random
    colors = PEGS.values
    randomized = []
    4.times { randomized << colors[rand(5)] }
    self.new(randomized)
  end

  def initialize(pegs)
    @pegs = pegs
  end

  def [](num)
    self.pegs[num]
  end

  def ==(other_code)
    return false if other_code == other_code.to_s
    self.pegs == other_code.pegs
  end

  def exact_matches(other_code)
    count = 0
    other_code.pegs.each_with_index do |peg, idx|
      count += 1 if other_code.pegs[idx] == self.pegs[idx]
    end
    count
  end

  def near_matches(other_code)
    count = 0
    our_pegs = self.pegs.dup
    user_pegs = other_code.pegs.dup
    user_pegs.each_with_index do |peg, idx|
      if our_pegs.include?(peg) && (user_pegs[idx] != our_pegs[idx])
        our_pegs.delete_at(our_pegs.index(peg))
        count += 1
      end
    end

    count
  end
end

class Game
  attr_reader :secret_code

  def initialize(code=Code.random)
    @secret_code = code
  end

#   def game_explanation
#     puts 'My secret code includes a sequence of four pegs of colors (RGBYOP).
# You shall try to guess it, and you have 10 tries. I\'ll tell you which
# matches are exact, and which are in the sequence but you missed the spot.
# Ready? Play!'
#   end

  def get_guess
    puts ' Give me your four colors.'
    guess = "BBBB"  #gets.chomp
    Code.parse(guess)
  end

  def display_matches(guess)
    print " exact matches: #{guess.exact_matches(@secret_code)}."
    print " near matches: #{guess.near_matches(@secret_code)}."
  end

#   def play
#     turns = 10
#     # game_explanation
#     until turns == 0
#       guess = get_guess
#       display_matches(guess)
#       turns -= 1
#       return 'NICE, YOU WIN!' if @secret_code.exact_matches(guess) == 4
#     end
#   end
end

# game = Game.new
# p game.play
